<?php

namespace App\Controller;

use App\Entity\Config;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Service\ConfigValidator;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class IndexController
 *
 * @package App\Controller
 */
class IndexController extends Controller
{
    /**
     * Statuses of client response
     */
    private const RESPONSE_STATUSES = [
        'success' => 0,
        'notAllRequiredFields' => 1,
        'unexpectedError' => 2,
    ];

    /**
     * @param Request $request
     * @param ConfigValidator $validator
     * @param ObjectManager $em;
     *
     * @Route("/save-config", name="save_config", methods={"POST"})
     *
     * @return Response
     */
    public function saveConfig(Request $request, ConfigValidator $validator, ObjectManager $em): Response
    {
        try {
            $config = $request->getContent();
            $status = self::RESPONSE_STATUSES['unexpectedError'];

            if (!empty($config)) {
                $status = $validator->validateConfig($config);

                if ($status === ConfigValidator::CONFIG_VALIDATION_STATUSES['success'] ||
                    $status === ConfigValidator::CONFIG_VALIDATION_STATUSES['error']
                ) {
                    $config = new Config();

                    $config
                        ->setRequest(json_encode($validator->getConfig()))
                        ->setStatus($status)
                    ;

                    $em->persist($config);
                    $em->flush();
                }
            }

            switch($status) {
                case ConfigValidator::CONFIG_VALIDATION_STATUSES['success']:
                    return new Response(self::RESPONSE_STATUSES['success']);
                case ConfigValidator::CONFIG_VALIDATION_STATUSES['error']:
                    return new Response(self::RESPONSE_STATUSES['notAllRequiredFields']);
                default:
                    return new Response(self::RESPONSE_STATUSES['unexpectedError']);
            }
        } catch (Exception $exception) {
            return new Response(self::RESPONSE_STATUSES['unexpectedError']);
        }
    }
}
