<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Database
 *
 * @package App\Entity
 *
 * @ORM\Table(name="config")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks
 */
class Config
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="request", type="json")
     */
    private $request;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="date_create", type="string")
     */
    private $dateCreate;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getRequest() : string
    {
        return $this->request;
    }

    /**
     * @param string $request
     *
     * @return Config
     */
    public function setRequest(string $request): self
    {
        $this->request = $request;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     *
     * @return Config
     */
    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string
     */
    public function getDateCreate(): string
    {
        return $this->dateCreate;
    }

    /**
     * @param string $dateCreate
     *
     * @return Config
     */
    public function setDateCreate(string $dateCreate): self
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     *  @ORM\PrePersist
     */
    public function setupDateCreate()
    {
        $this->dateCreate = date('Y-m-d H:i:s');
    }
}
