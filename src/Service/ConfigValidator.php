<?php

namespace App\Service;

/**
 * Class ConfigValidator
 *
 * @package App\Service
 */
class ConfigValidator
{
    /** @var array */
    private $config;

    /**
     * @return array
     */
    public function getConfig(): array
    {
        return $this->config;
    }

    /**
     * Required json config properties
     */
    private const REQUIRED_CONFIG_PROPERTIES = [
        'CURRENT',
        'GOAL',
        'MAX',
    ];

    /**
     * Statuses of config validation
     */
    public const CONFIG_VALIDATION_STATUSES = [
        'success'         => 1,
        'error'           => 2,
        'unexpectedError' => 3,
    ];

    /**
     * @param string $config
     *
     * @return int
     */
    public function validateConfig(string $config): int
    {
        try {
            $this->config = json_decode($config, true);

            $this->filterRedundantProperties($this->config);

            if (empty($this->config)) {
                return self::CONFIG_VALIDATION_STATUSES['unexpectedError'];
            }

            if (count($this->config) < count(self::REQUIRED_CONFIG_PROPERTIES)) {
                return self::CONFIG_VALIDATION_STATUSES['error'];
            }

            return self::CONFIG_VALIDATION_STATUSES['success'];
        } catch (\Exception $e) {
            return self::CONFIG_VALIDATION_STATUSES['unexpectedError'];
        }
    }

    /**
     * @param array $config
     */
    private function filterRedundantProperties(array &$config): void
    {
        foreach ($config as $key => $property) {
            if (!in_array($key, self::REQUIRED_CONFIG_PROPERTIES)) {
                unset($config[$key]);
            }
        }
    }
}
