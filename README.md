Server Test Application
========================

(!)Make sure that installed PHP version is 7.1.3 or higher
--------------

Install application:
--------------

```
composer install
```

- Setup database connection in .env file
- Create database and schema using:
```
php bin/console doctrine:database:create
php bin/console doctrine:schema:create
```

or do it manually.

Run built-in web server 
```
php bin/console server:run
```

or use any other available
